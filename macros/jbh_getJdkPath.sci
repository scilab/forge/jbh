// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function jdkPath = jbh_getJdkPath()
  jdkPath = getenv("JBH_JDK_HOME", "");
  if jdkPath <> "" then
    jdkPath = fullpath(jdkPath + filesep());
  else
    jdkPath = [];
  end
endfunction
// =============================================================================
