// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function jbhPath = jbh_getJbhPath()
  jbhPath = [];
  [macrosname, jbhpath] = libraryinfo("jbhlib");
  jbhPath = fullpath(jbhpath + filesep());
endfunction
// =============================================================================
