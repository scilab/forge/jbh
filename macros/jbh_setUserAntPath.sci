// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function bRes = jbh_setUserAntPath(myAntPath)
  bRes = %F;
  
  if isempty(myAntPath) then
    if isfile(SCIHOME + "/jbh_ant_conf.txt") then
      bRes = deletefile(SCIHOME + "/jbh_ant_conf.txt");
    end
  else 
    if size(myAntPath, "*") <> 1 then
      error(999, msprintf(gettext("%s: Wrong size for input argument #%d: A directory name expected.\n")), "jbh_setJdkPath", 1);
    end
  
    if type(myAntPath) <> 10 then
      error(999, msprintf(gettext("%s: Wrong type for input argument #%d: A directory name expected.\n")), "jbh_setJdkPath", 1);  
    end
  
    if ~isdir(myAntPath) then
      error(999, msprintf(gettext("%s: Wrong value for input argument #%d: A existing directory name expected.\n")), "jbh_setJdkPath", 1);
    end
  
    bRes = mputl(myAntPath, SCIHOME + "/jbh_ant_conf.txt");
  end 
endfunction
// =============================================================================
