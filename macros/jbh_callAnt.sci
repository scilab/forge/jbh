// =============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// =============================================================================
function [bRes, txt] = jbh_callAnt(param_call_ant)

  if ~isdef("param_call_ant") then
    param_call_ant = "";
  end
 
  if size(param_call_ant, "*") <> 1 then
    error(999, msprintf(gettext("%s: Wrong size for input argument #%d: A string expected.\n")), "jbh_callAnt", 1);
  end
  
  if type(param_call_ant) <> 10 then
    error(999, msprintf(gettext("%s: Wrong type for input argument #%d: A string expected.\n")), "jbh_callAnt", 1);  
  end
    
  [txt_unix_g, stat_unix_g] = unix_g("ant " + param_call_ant);
  if stat_unix_g <> 0 then
    bRes = %F;
  else
    bRes = %T;
  end
  
  txt = txt_unix_g;
endfunction
// =============================================================================
